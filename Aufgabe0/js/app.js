/*
 * USES THE HTML5 CLASSLIST API. MAY NOT WORK IN ALL BROWSERS.
 * IE < 10 not supported without shim
 */
(function () {

/**
 * Constructs a fade toggle function for use in a callback
 */
function makeFade(el, duration, cb){
  return function(){
    if(el.classList.contains("animating")){
      return;
    }
    el.classList.add("animating");
    el.classList.remove("hidden");
    window.requestAnimationFrame(function(){
      if(el.classList.toggle("invisible")){
        setTimeout(function() {
          el.classList.add("hidden");
          el.classList.remove("animating");
          if(cb){cb();}
        }, duration);
      }else{
        setTimeout(function(){
          el.classList.remove("animating");
          if(cb){cb();}
        }, duration);
      }
    });
  };
}

var article = document.querySelectorAll('article')[0],
    detail = document.querySelectorAll('.detail')[0];

article.querySelectorAll('.weiter-lesen')[0].onclick = makeFade(article, 2000, makeFade(detail, 2000));
document.querySelectorAll('.link-back')[0].onclick = makeFade(detail, 2000, makeFade(article, 2000));

}());
