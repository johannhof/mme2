var databaseUrl = "mme2db";

// use two collections:
// - topicview will store the whole configuration of a topicview (e.g. 'Die Umsiedlerin')
// - objects will store all "objects" that will be displayed on some topicview
var collections = ["topicviews", "objects"];

var db = require("mdbjs").connect(databaseUrl, collections);
var utils = require("./njsutils");
var urlTools = require('url');
var ObjectID = require('mdbjs').ObjectId;

/* MFM: import the components required for multipart request ("file upload") processing
 the class must be imported like this, otherwise its instances will not keep their state */
var MultipartReader = require("./multipart").MultipartReader;
var multipart = require("./multipart");

module.exports = {

  processRequest : function processRequest(req, res) {
    var query;
    try {
      query = buildQuery(urlTools.parse(req.url, true));
    } catch (e) {
      // if the request is malformed
      return respondError(res, 400);
    }

    processByMethod[req.method](query, req,res);
  }
};

function buildQuery(url){
  var path = utils.substringAfter(url.pathname, "http2mdb").split("/");
  var query = {search: url.query || {}, resource: path[1]};

  if(!~collections.indexOf(query.resource)){
    throw new Error("Specified resource not found.");
  }

  for (var key in query.search) {
    if (query.search.hasOwnProperty(key) && query.search[key]) {
      var values = query.search[key].split(',');
      if(key === "_id"){
        values = values.map(function (id) {
          return ObjectID(id);
        });
      }
      if(values.length > 1 || key === "_id"){
        query.search[key] = {$in : values};
      }
    }
  }

  if(path[2] && !query._id){
    query.search._id = ObjectID(path[2]);
  }
  return query;
}

var processByMethod = {

  GET : function (query, req, res) {
    db[query.resource].find(query.search, function(err, elements) {
      if (err || !elements) {
        console.log("Error accessing topicview!");
        respondError(res);
      }else{
        respondSuccess(res, elements);
      }
    });
  },

  PUT : function (query, req, res) {
    // we read out the data and then update the db with the data being passed
    req.on("data", function(data) {
      console.log("PUT: data is: " + data);
      // and update it to the topicviews collection - note that we can directly pass the data received to the update function
      db[query.resource].update(query.search, { $set : JSON.parse(data) }, function(err, updated) {
        if (err || !updated) {
          respondError(res);
        } else {
          respondSuccess(res, 204);
        }
      });
    });
  },

  POST : function (query, req, res) {

    // MFM: check whether we have a multipart request
    if (utils.startsWith(req.headers["content-type"], "multipart/form-data;")) {
      return multipart.handleMultipartRequest(req, res, "./webcontent/", "content/", function ondone(formdata) {
          respondSuccess(res, formdata, 201);
      });
    }

    req.on("data", function(data) {
      console.log("createTopicview(): data is: " + data);

      db[query.resource].save(JSON.parse(data), function(err, saved) {
        if (err || !saved) {
          console.error("topic data could not be saved: " + err);

          respondError(res);
        } else {
          console.log("createTopicview(): saved object is: " + JSON.stringify(saved));

          respondSuccess(res, saved, 201);
        }
      });
    });
  },

  DELETE : function (query, req, res) {
    db[query.resource].remove(query.search, function(err, update) {
      if (err || !update) {
        console.log("topicview could not be deleted. Got: " + err);
        respondError(res);
      } else {
        console.log("topicview was deleted. Got: " + update);
        respondSuccess(res, 204);
      }
    });
  }
};

function respondSuccess(res, json, code) {
  // swap variables for pseudo function overloading
  if(typeof json === "number"){
    code = json;
    json = undefined;
  }

  if (json) {
    res.writeHead(code || 200, {
      'Content-Type' : 'application/json'
    });
    res.write(JSON.stringify(json));
  } else {
    res.writeHead(code || 200);
  }

  res.end();
}

function respondError(res, code) {
  res.writeHead(code || 500);
  res.end();
}
