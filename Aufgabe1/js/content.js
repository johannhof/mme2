(function () {

  /**
   * Small templating function.
   *
   * JS can be evaluated by inserting the tag pairs
   * <~ console.log('and') ~>
   *
   * To interpolate use the tags <+ and +>. Variable names can be properties
   * of the data object that is given to the function returned by tmpl.
   *
   * Do not use single quotes (' ') in templates!
   *
   * @param {String} template an html based template
   *
   * @return {Function} a function that builds the final pure html string. Can be called with a data object
   */
  function tmpl(template) {
    return new Function(
      "data",
      "var tmpl = []; with(data){tmpl.push('" +
        template.replace(/[\r\t\n]/g, " ")
                .replace(/<\+/g, "');tmpl.push(")
                .replace(/\+>/g, ");tmpl.push('")
                .replace(/<~/g, "');")
                .replace(/~>/g, "tmpl.push('") +
        "');}return tmpl.join('');");
  }

  /**
   * Creates an html string from an html-based string template and a data object
   *
   * @param {Object} data the rendering data
   *
   * @return {String} an html structure as a string
   */
  var render = {
    objekt: tmpl(document.getElementById("objekt-tmpl").innerHTML),
    textauszug: tmpl(document.getElementById("textauszug-tmpl").innerHTML),
    medienverweise: tmpl(document.getElementById("verknuepfungen-tmpl").innerHTML)
  };

  function createContent(contentItem) {
    // should we load the content or is it already there?
    if(contentItem.render_async){
      xhr("GET", contentItem.src, null, function(xmlhttp) {
        populateDOMNode(contentItem.render_container, xmlhttp, contentItem.type);
      });
    }else{
      populateDOMNode(contentItem.render_container, contentItem);
    }
  }

  /**
   * Fills the node with the specified id with the rendered template
   */
  function populateDOMNode(containerID, content, type) {
    type = type || content.type;
    if(render[type]){
      document.getElementById(containerID).innerHTML += render[type](content);
    }
  }

  window.loadContentFromServer = function(url) {
    xhr("GET", url, null, function(xmlhttp) {
      var jsonContent = JSON.parse(xmlhttp.responseText);
      document.getElementById("topic_title").textContent = jsonContent.title;

      jsonContent.content_items.forEach(createContent);
    });
  };
}());
