(function () {

  window.onpopstate = function (event) {
    if(location.hash && location.hash.slice(1)){
      loadContentFromServer("http2mdb/topicviews/?topicid=" + location.hash.slice(1));
    }
  };

  // firefox doesnt fire onpopstate at page load
  if(~navigator.userAgent.toLowerCase().indexOf('firefox')){
    window.onpopstate();
  }

  loadTitlelist();
}());
