/*
 * get the base url
 * see http://stackoverflow.com/questions/9563876/base-url-using-jquery
 *
 * we need to distinguish here between applications that have a root context and ones where the root is the host - hard-code it for the time being within the applications...
 */
function getBaseUrl() {
  // the full url as shown in the browser window
  var l = window.location;

  // the host url
  return l.protocol + "//" + l.host + "/";
}

/*
 * generic method for calling a webapp passing / receiving json and callback methods
 *
 * method: the http method to be executed
 * requestpath: the path to be appended to the root path of the webapp
 * obj: an (optional object to be passed)
 * onsucccess: callback in case of success
 * onerror: callback in case of error
 */
function xhr(method, requestpath, obj, onsuccess, onerror) {

  var xmlhttp = new XMLHttpRequest();
  var url = null;

  if (!requestpath) {
    return console.error("no requestpath specified! Ignore...");
  }

  // check whether we have a full or a relative url
  if (requestpath.indexOf("http://") === 0 || requestpath.indexOf("https://") === 0) {
    url = requestpath;
  } else {
    url = getBaseUrl() + requestpath;
  }

  /*
   * specify the callback function using our own callback function arguments - this code will be executed as soon as we have started sending the request
   */
  xmlhttp.onreadystatechange = function() {

  if(xmlhttp.readyState === 4) {
    console.log("onreadstatechange: request finished and response is ready. Status is: " + xmlhttp.status);
    // in case we have a request code of 200 OK, we execute the onsuccess function passed as an argument
    if (xmlhttp.status > 100 && xmlhttp.status < 300) {
      // show how to access a response header
      console.log("response Content-Type header is: " + xmlhttp.getResponseHeader("Content-type"));
      console.log("responseType is: " + xmlhttp.responseType);
      console.log("response is: " + xmlhttp.response);

      if (onsuccess) {
        // the function will be passed the request object to be free with regard to reading out its content
        onsuccess(xmlhttp);
      }
    } else {
      if (onerror) {
        onerror(xmlhttp);
      }
    }
  }
};

xmlhttp.open(method, url, true);

// if the data is not already packed in a suitable format
if(!(obj instanceof FormData)){
  if (obj) {
    obj = JSON.stringify(obj);
    xmlhttp.setRequestHeader("Content-type", "application/json");
  }
}

// set the header indicating which content types we accept (quite coarse-grained, though)
xmlhttp.setRequestHeader("Accept", "application/json, application/xml, text/html, text/plain");

// send the request and pass the json string as content or do not pass any content
console.log("sending request...");
if (obj) {
  xmlhttp.send(obj);
} else {
  xmlhttp.send();
}
}
