/**
 * @author Jörn Kreutel
 */
// a variable that holds the basepath of the webapp
var webapp_basepath = getBaseUrl();

/*
 * get the base url
 * see http://stackoverflow.com/questions/9563876/base-url-using-jquery
 *
 * we need to distinguish here between applications that have a root context and ones where the root is the host - hard-code it for the time being within the applications...
 */
function getBaseUrl() {
  // the full url as shown in the browser window
  var l = window.location;

  // the host url
  var base_url = l.protocol + "//" + l.host + "/";

  // append the base context of the webapp in case more than one application is served by the host (which is the case as long as we let our applications be served by aptana)
  base_url += (l.pathname != "/" && l.pathname != "" ? l.pathname.split('/')[1] + "/" : "");

  console.log("getBaseUrl(): " + base_url);

  return base_url;
}

/*
 * generic method for calling a webapp passing / receiving json and callback methods
 *
 * method: the http method to be executed
 * requestpath: the path to be appended to the root path of the webapp
 * obj: an (optional object to be passed)
 * onsucccess: callback in case of success
 * onerror: callback in case of error
 */
function xhr(method, requestpath, obj, onsuccess, onerror) {
  console.log("callWebapp()");

  // create the request
  var xmlhttp = new XMLHttpRequest();

  var url = null;

  if (requestpath) {

    // check whether we have a full or a relative url
    if (!requestpath.indexOf("http://") || !requestpath.indexOf("https://")) {
      url = requestpath;
    } else {
      url = webapp_basepath + requestpath;
    }

    /*
     * specify the callback function using our own callback function arguments - this code will be executed as soon as we have started sending the request
     */
    xmlhttp.onreadystatechange = function() {

      switch (xmlhttp.readyState) {
        case 4:
          console.log("onreadstatechange: request finished and response is ready. Status is: " + xmlhttp.status);
        // in case we have a request code of 200 OK, we execute the onsuccess function passed as an argument
        if (xmlhttp.status == 200) {
          // show how to access a response header
          console.log("response Content-Type header is: " + xmlhttp.getResponseHeader("Content-type"));
          console.log("responseType is: " + xmlhttp.responseType);
          console.log("response is: " + xmlhttp.response);

          if (onsuccess) {
            // the function will be passed the request object to be free with regard to reading out its content
            onsuccess(xmlhttp);
          } else {
            alert("request " + url + " executed successfully, but no onsuccess callback is specified.");
          }
        } else {
          if (onerror) {
            onerror(xmlhttp);
          } else {
            alert("got error processing request " + url + ", but no onerror callback is specified. Status code is: " + xmlhttp.status);
          }
        }
        break;
        // we add log messages for the other status
        case 3:
          console.log("onreadstatechange: processing response...");
        break;
        case 2:
          console.log("onreadstatechange: response received.");
        break;
        case 1:
          console.log("onreadstatechange: connection established.");
        break;
        case 0:
          console.log("onreadstatechange: request not initialised yet.");
        break;
      }
    };


    //open a connection to the server
    xmlhttp.open(method, url, true);

    // set the header indicating which content types we accept (quite coarse-grained, though)
    xmlhttp.setRequestHeader("Accept", "application/json, application/xml, text/html, text/plain");

    // send the request and pass the json string as content or do not pass any content
    console.log("sending request...");
    if (obj) {
      // set the header that indicates what type of content we are sending
      xmlhttp.setRequestHeader("Content-type", "application/json");
      xmlhttp.send(JSON.stringify(obj));
    } else {
      xmlhttp.send();
    }
  } else {
    console.error("no requestpath specified! Ignore...");
  }
}
