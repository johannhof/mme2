module.exports = {};

/*
 * functions that shall also be used internally can be declared like this:
 */
module.exports.startsWith = function startsWith(string, substring) {
  return !string.indexOf(substring);
};

module.exports.endsWith = function endsWith(string, substring) {
  return string.length >= substring.length && string.substring(string.length - substring.length) == substring;
};

module.exports.getIPAddress = function getIPAddress() {
  var interfaces = require('os').networkInterfaces();
  for (var devName in interfaces) {
    var iface = interfaces[devName];

    for (var i = 0; i < iface.length; i++) {
      var alias = iface[i];
      if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal){
        return alias.address;
      }
    }
  }

  return '127.0.0.1';
};

module.exports.substringAfter = function(string, separator) {
  return string.slice(string.indexOf(separator) + separator.length);
};

module.exports.trimQuotes = function trimQuotes(string) {
  var trimmed = string.trim();
  if ((module.exports.startsWith(trimmed, "\"") && module.exports.endsWith(trimmed, "\"")) || (module.exports.startsWith(trimmed, "\'") && module.exports.endsWith(trimmed, "\'"))) {
    return trimmed.substring(1, trimmed.length - 1);
  }
  return string;
};
