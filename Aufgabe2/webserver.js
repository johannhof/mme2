/*
 * taken from http://jmesnil.net/weblog/2010/11/24/html5-web-application-for-iphone-and-ipad-with-node-js/
 */

var http = require('http');
var url = require('url');
var fs = require('fs');
var sys = require('sys');

var utils = require("./njsimpl/njsutils");

var http2mdb = require("./njsimpl/http2mdb");

// the port on which the server will be started
var port = 4500;

// the ip address
//var ip = utils.getIPAddress();
var ip = "127.0.0.1";

var server = http.createServer(function(req, res) {
  var path = url.parse(req.url).pathname;

  console.log("http request callback: trying to serve path: " + path);

  // check whether we have an api call or need to serve a file
  if (!path.indexOf("/http2mdb/")) {
    console.log("http request callback: got a call to the http2mdb api. Will continue processing there...");
    http2mdb.processRequest(req, res);
  } else {
    if (path == '/') {
      // if the root is accessed we serve the main html document
      path = "titlelist.html";
    }
    // serveable resources will be put in the webcontent directory -- the callback will be passed the data read out from the file being accessed
    fs.readFile(__dirname + "/webcontent/" + path, function(err, data) {
      // check whether we have got an error retrieving the resource: create a 404 error, assuming that a wrong uri was used
      if (err) {
        res.writeHead(404);
        res.end();
      }
      // otherwise create a 200 response and set the content type header
      else {
        res.writeHead(200, {
          'Content-Type' : contentType(path)
        });
        res.write(data, 'utf8');
        res.end();
      }
    });
  }
});

// let the server listen on the given port
server.listen(port, ip);
console.log("HTTP server running at http://" + ip + ":" + port);

/*
 * helper method for assiging a Content-Type header to http responses
 */
function contentType(path) {
  if (path.match('.js$')) {
    return "text/javascript";
  } else if (path.match('.css$')) {
    return "text/css";
  } else if (path.match('.json$')) {
    return "application/json";
  } else if (path.match('.css$')) {
    return "text/css";
  } else if (path.match('.png$')) {
    return "image/png";
  } else if (path.match('.ogv$')) {
    return "video/ogg";
  } else if (path.match('.ogg$')) {
    return "audio/ogg";
  } else if (path.match('.manifest$')) {
    return "text/cache-manifest";
  } else if (path.match('.webapp$')) {
    return "application/x-web-app-manifest+json";
  } else {
    return "text/html";
  }
}

